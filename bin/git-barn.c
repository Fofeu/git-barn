#include "barn_fuse.h"
#include "barn_options.h"
#include "barn_operations.h"

#include <stdlib.h>
#include <assert.h>

int main(int argc, char *argv[])
{
  int ret;
  struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

  barn_options_init();

  if (fuse_opt_parse(&args, &barn_options, barn_option_spec, NULL) == -1)
    return EXIT_FAILURE;

  if (barn_options.show_help) {
    barn_show_help(argv[0]);
    assert(fuse_opt_add_arg(&args, "--help") == 0);
    args.argv[0][0] = '\0';
  }

  ret = fuse_main(args.argc, args.argv, &barn_operations, NULL);

  fuse_opt_free_args(&args);

  return ret;
}
