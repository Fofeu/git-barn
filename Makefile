EXECS := git-barn

git-barn_LIBS := libgit2 fuse3
git-barn_SRCS := bin/git-barn.c src/barn_options.c src/barn_operations.c src/barn_utils.c

include Makefiles/Makefile
