#pragma once

enum barn_error {
  EBARN_OK = 0,
  EBARN_OTHER = -1,
  EBARN_NO_MEM = -2,
  EBARN_NOT_FOUND = -3,
  EBARN_NOT_TREE = -4,
  EBARN_NOT_BLOB = -5,
  EBARN_LIBGIT2 = -6,

};
