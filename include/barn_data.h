#pragma once

#include <git2.h>

struct barn_data
{
  git_repository* repo;
  git_tree* root;
};
