#pragma once

#include "barn_fuse.h"
#include "barn_data.h"
#include "barn_error.h"

#include <git2.h>

enum barn_error barn_lookup(git_tree_entry** entry, const char* path, struct barn_data* repo);

enum barn_error barn_lookup_tree(git_tree** tree, const char* path, struct barn_data* barn_data);
enum barn_error barn_lookup_tree_byentry(git_tree** tree, git_tree_entry* path, struct barn_data* barn_data);

enum barn_error barn_lookup_blob(git_blob** blob, const char* path, struct barn_data* barn_data);
enum barn_error barn_lookup_blob_byentry(git_blob** tree, git_tree_entry* path, struct barn_data* barn_data);

int barn_getattr_tree(struct stat* stbuf, git_tree* tree, git_repository* repo);
int barn_getattr_blob(struct stat* stbuf, git_blob* blob, git_repository* repo);

#if __STDC_VERSION__ < 202311L

#ifdef __GNUC__ // GCC, Clang, ICC
#define unreachable() (__builtin_unreachable())
#endif

#ifdef _MSC_VER // MSVC
#define unreachable() (__assume(false))
#endif

#ifndef unreachable
#error "Unable to define unreachable macro"
#endif

#endif
