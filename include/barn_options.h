#pragma once

#include "barn_fuse.h"

struct barn_options {
  const char *git_path;
  const char *branch;
  int show_help;
};

extern struct barn_options barn_options;
extern const struct fuse_opt barn_option_spec[];

void barn_options_init(void);

void barn_show_help(const char *progname);
