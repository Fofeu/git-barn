#include "barn_fuse.h"
#include "barn_data.h"
#include "barn_options.h"
#include "barn_utils.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <git2.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stddef.h>

static void* barn_init    (struct fuse_conn_info* conn, struct fuse_config* cfg);
static void  barn_destroy (void* private_data);
static int   barn_access  (const char* path, int amode);
static int   barn_getattr (const char* path, struct stat* stbuf, struct fuse_file_info* fi);
static int   barn_opendir (const char* path, struct fuse_file_info* fi);
static int   barn_readdir (const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi, enum fuse_readdir_flags flags);

struct barn_dirp {
  git_tree* treep;
  const git_tree_entry* entry;
  off_t offset;
};

const struct fuse_operations barn_operations = {
    .init = barn_init,
    .destroy = barn_destroy,
    .access = barn_access,
    .getattr = barn_getattr,
    .opendir = barn_opendir,
    .readdir = barn_readdir,
};

static void *barn_init(struct fuse_conn_info *conn, struct fuse_config *cfg)
{
  (void)conn,(void)cfg;

  int git_error_code;
  struct barn_data* barn_data = NULL;
  git_reference* branch = NULL;
  git_object* root = NULL;

  git_libgit2_init();

  git_repository* repo = NULL;

  git_error_code = git_repository_open_bare(&repo, barn_options.git_path);
  if (git_error_code != 0)
  {
    const git_error *e = git_error_last();
    printf("Error git-barn/libgit2 %d/%d: %s\n", git_error_code, e->klass, e->message);
    goto failure;
  }

  git_error_code = git_branch_lookup(&branch, repo, barn_options.branch, GIT_BRANCH_LOCAL);
  if (git_error_code != 0)
  {
    const git_error *e = git_error_last();
    printf("Error git-barn/libgit2 %d/%d: %s\n", git_error_code, e->klass, e->message);
    goto failure;
  }

  git_error_code = git_reference_peel(&root, branch, GIT_OBJECT_TREE);
  if (git_error_code != 0)
  {
    const git_error *e = git_error_last();
    printf("Error git-barn/libgit2 %d/%d: %s\n", git_error_code, e->klass, e->message);
    goto failure;
  }
  if (git_object_type(root) != GIT_OBJECT_TREE)
  {
    printf("Error git-barn/logic: Root didn't dereference into a tree.\n");
    goto failure;
  }

  barn_data = malloc(sizeof(struct barn_data));

  if (barn_data == NULL)
  {
    goto failure;
  }

  barn_data->repo = repo;
  barn_data->root = (git_tree*)root;


  return barn_data;

failure:
  if (branch)
    git_reference_free(branch);
  if (root)
    git_object_free(root);
  if (repo)
    git_repository_free(repo);
  if (barn_data)
    free(barn_data);

  return NULL;
}

static void barn_destroy(void *private_data)
{
  struct barn_data* barn_data = private_data;

  if (barn_data)
  {
    if (barn_data->repo)
    {
      git_repository_free(barn_data->repo);
    }
  }

  git_libgit2_shutdown();

}

static int barn_access(const char *path, int amode)
{
  git_tree* tree = NULL;
  struct fuse_context* ctx = fuse_get_context();
  struct barn_data* barn_data = (struct barn_data*)ctx->private_data;
  int result;

  enum barn_error barn_error = barn_lookup_tree(&tree, path, barn_data);

  switch (barn_error)
  {
  case EBARN_OK:
    result = 0;
    break;

  case EBARN_NOT_FOUND:
  case EBARN_LIBGIT2:
    result = -ENOENT;
    break;

  case EBARN_NOT_TREE:
    result = -ENOTDIR;
    break;

  default:
    unreachable();
    break;
  }
  git_tree_free(tree);

  //No write rights as of now
  if (amode & W_OK)
    result = -EACCES;

  // We don't do any other checks because we're either checking for (existence || (read right | exec right))
  // We checked existence earlier and read/exec rights are always available
  return result;
}

static int barn_getattr(const char* path, struct stat* stbuf, struct fuse_file_info* fi)
{
  (void)fi;

  git_tree_entry* entry = NULL;
  git_tree* tree = NULL;
  git_blob* blob = NULL;
  enum barn_error barn_error;
  const git_error* git_error;
  struct fuse_context* ctx = fuse_get_context();
  struct barn_data* barn_data = (struct barn_data*)ctx->private_data;

  memset(stbuf, 0, sizeof(struct stat));
  if (strcmp(path, "/") == 0)
  {
    return barn_getattr_tree(stbuf, barn_data->root, barn_data->repo);
  }

  barn_error = barn_lookup(&entry, path, barn_data);

  switch (git_tree_entry_type(entry))
  {
  case GIT_OBJECT_TREE:
    barn_error = barn_lookup_tree_byentry(&tree, entry, barn_data);
    git_tree_entry_free(entry);
    switch (barn_error)
    {
    case EBARN_OK:
      return barn_getattr_tree(stbuf, tree, barn_data->repo);
      break;
    case EBARN_LIBGIT2:
      git_error = git_error_last();
      printf("Error git-barn/libgit2 %d: %s\n", git_error->klass, git_error->message);
      if (tree)
        git_tree_free(tree);
      return -ENOENT;
      break;
    default:
      unreachable();
      break;
    }
    break;

  case GIT_OBJECT_BLOB:
    barn_error = barn_lookup_blob_byentry(&blob, entry, barn_data);
    git_tree_entry_free(entry);
    switch (barn_error)
    {
    case EBARN_OK:
      return barn_getattr_blob(stbuf, blob, barn_data->repo);
      break;
    case EBARN_LIBGIT2:
      git_error = git_error_last();
      printf("Error git-barn/libgit2 %d: %s\n", git_error->klass, git_error->message);
      if (blob)
        git_tree_free(tree);
      return -ENOENT;
      break;
    default:
      unreachable();
      break;
    }
    break;

  default:
    git_tree_entry_free(entry);
    return -ENOENT;
  }

}

static int barn_opendir(const char* path, struct fuse_file_info* fi)
{
  static_assert(sizeof(struct barn_dirp*) <= sizeof(((struct fuse_file_info*)NULL)->fh), "Can't store a pointer inside a uint64_t. Function barn_opendir is thus unsafe.");

  struct barn_dirp* fh;
  struct fuse_context* ctx = fuse_get_context();
  struct barn_data* barn_data = (struct barn_data*) ctx->private_data;
  struct git_tree* tree;
  const git_error* git_error;
  enum barn_error barn_error;

  fh = malloc(sizeof(struct barn_dirp*));
  if (fh == NULL)
    return -ENOMEM;

  barn_error = barn_lookup_tree(&tree, path, barn_data);

  switch (barn_error)
  {
  case EBARN_OK:
    break;

  case EBARN_LIBGIT2:
    git_error = git_error_last();
    printf("Error git-barn/libgit2 %d: %s\n", git_error->klass, git_error->message);
    if (tree)
      git_tree_free(tree);
    return -ENOENT;
    break;

  case EBARN_NOT_TREE:
    if (tree)
      git_tree_free(tree);
    return -ENOTDIR;
    break;

  default:
    unreachable();
    break;
  }

  fh->treep = tree;
  fh->entry = NULL;
  fh->offset = 0;

  fi->fh = (uint64_t)(uintptr_t)fh;

  return 0;

}

static int barn_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, enum fuse_readdir_flags flags)
{
  (void)path,(void)buf,(void)filler,(void)offset,(void)fi,(void)flags;
  struct barn_dirp* dirp = (struct barn_dirp*)(uintptr_t)fi->fh;

  if (offset != dirp->offset)
  {
    dirp->entry = NULL;
    dirp->offset = offset;
  }

  while (1)
  {

    struct stat stat;
    enum fuse_fill_dir_flags fill_flags = 0;

    if (!dirp->entry)
    {
      dirp->entry = git_tree_entry_byindex(dirp->treep, dirp->offset);
      if (!dirp->entry)
        break;
    }

    if (flags & FUSE_READDIR_PLUS)
    {
      //TODO
      (void)stat;
    }

    //We increment it here because filler starts counting at 0
    dirp->offset += 1;

    int res = filler(buf, git_tree_entry_name(dirp->entry), NULL, dirp->offset, fill_flags);

    if (res != 0)
      break;
    dirp->entry = NULL;
  }

  return 0;
}
