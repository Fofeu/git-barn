#ifdef __STDC_ALLOC_LIB__
#define __STDC_WANT_LIB_EXT2__ 1
#else
#define _POSIX_C_SOURCE 200809L
#endif

#include "barn_options.h"

#include <string.h>
#include <stdio.h>
#include <stddef.h>

struct barn_options barn_options = {0};

#define OPTION(t, p) { t, offsetof(struct barn_options, p), 1 }
const struct fuse_opt barn_option_spec[] = {
        OPTION("--git-path=%s", git_path),
        OPTION("--branch=%s", branch),
        OPTION("-h", show_help),
        OPTION("--help", show_help),
        FUSE_OPT_END
};
#undef OPTION

void barn_options_init(void)
{
  barn_options.branch = strdup("main");
}

void barn_show_help(const char *progname)
{
        printf("usage: %s [options] <mountpoint>\n\n", progname);
        printf("File-system specific options:\n"
               "    --git-path=<s>      Path to the (bare) git repository\n"
               "    --branch=<s>        Name of the mounted branch\n"
               "                        (default \"main\")\n"
               "\n");
}
