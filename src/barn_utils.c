#define _XOPEN_SOURCE 700

#include "barn_utils.h"

#include <stdlib.h>
#include <string.h>
#include <git2.h>
#include <sys/stat.h>

enum barn_error barn_lookup(git_tree_entry** entry, const char* path, struct barn_data* barn_data)
{
  //We assume paths provided by the kernel are valid
  //Paths should be /path/to/thing
  //libgit2 expects paths to be path/to/thing
  //Thus we must special case the / path
  //Since we can't construct a git_tree_entry, this lookup fails
  if (strcmp(path, "/") == 0)
    return EBARN_OTHER;
  int git_error_code = git_tree_entry_bypath(entry, barn_data->root, path+1);
  if (git_error_code != 0)
  {
    if (*entry)
      git_tree_entry_free(*entry);
    if (git_error_code == GIT_ENOTFOUND)
      return EBARN_NOT_FOUND;
    else
      return EBARN_LIBGIT2;
  }

  return EBARN_OK;
}

enum barn_error barn_lookup_tree_byentry(git_tree** tree, git_tree_entry* entry, struct barn_data* barn_data)
{
  int git_error_code = git_tree_lookup(tree, barn_data->repo, git_tree_entry_id(entry));
  if (git_error_code != 0)
  {
    if (*tree)
      git_tree_free(*tree);
    return EBARN_LIBGIT2;
  }

  return EBARN_OK;
}

enum barn_error barn_lookup_blob_byentry(git_blob** blob, git_tree_entry* entry, struct barn_data* barn_data)
{
  int git_error_code = git_blob_lookup(blob, barn_data->repo, git_tree_entry_id(entry));
  if (git_error_code != 0)
  {
    if (*blob)
      git_blob_free(*blob);
    return EBARN_LIBGIT2;
  }

  return EBARN_OK;
}

enum barn_error barn_lookup_tree(git_tree** tree, const char* path, struct barn_data* barn_data)
{
  //We assume paths provided by the kernel are valid
  //Paths should be /path/to/thing
  //libgit2 expects paths to be path/to/thing
  //Thus we must special case the / path
  if (strcmp(path, "/") == 0)
  {
    *tree = barn_data->root;
    return EBARN_OK;
  }

  //Not root so we perform lookup, also we increment path pointer to skip the leading '/'
  git_tree_entry* entry = NULL;
  int git_error_code = git_tree_entry_bypath(&entry, barn_data->root, path+1);
  if (git_error_code != 0)
  {
    if (entry)
      git_tree_entry_free(entry);
    if (git_error_code == GIT_ENOTFOUND)
      return EBARN_NOT_FOUND;
    else
      return EBARN_LIBGIT2;
  }

  if (git_tree_entry_type(entry) != GIT_OBJECT_TREE)
  {
    git_tree_entry_free(entry);
    return EBARN_NOT_TREE;
  }

  git_error_code = git_tree_lookup(tree, barn_data->repo, git_tree_entry_id(entry));
  git_tree_entry_free(entry);
  if (git_error_code != 0)
  {
    if (*tree)
      git_tree_free(*tree);
    return EBARN_LIBGIT2;
  }

  return EBARN_OK;
}

enum barn_error barn_lookup_blob(git_blob **blob, const char *path, struct barn_data *barn_data)
{
  //We assume paths provided by the kernel are valid
  //Paths should be /path/to/thing
  //libgit2 expects paths to be path/to/thing
  //Just to be sure, we double-check that we are required to fetch root as a blob
  if (strcmp(path, "/") == 0)
  {
    *blob = NULL;
    return EBARN_NOT_BLOB;
  }

  //Not root so we perform lookup, also we increment path pointer to skip the leading '/'
  git_tree_entry* entry = NULL;
  int git_error_code = git_tree_entry_bypath(&entry, barn_data->root, path+1);
  if (git_error_code != 0)
  {
    if (entry)
      git_tree_entry_free(entry);
    if (git_error_code == GIT_ENOTFOUND)
      return EBARN_NOT_FOUND;
    else
      return EBARN_LIBGIT2;
  }

  if (git_tree_entry_type(entry) != GIT_OBJECT_BLOB)
  {
    git_tree_entry_free(entry);
    return EBARN_NOT_BLOB;
  }

  git_error_code = git_blob_lookup(blob, barn_data->repo, git_tree_entry_id(entry));
  git_tree_entry_free(entry);
  if (git_error_code != 0)
  {
    if (*blob)
      git_blob_free(*blob);
    return EBARN_LIBGIT2;
  }

  return EBARN_OK;
}

int barn_getattr_tree(struct stat* stbuf, git_tree* tree, git_repository* repo)
{
  (void)tree,(void)repo;
  stbuf->st_mode = S_IFDIR | 0555;
  stbuf->st_nlink = 2;
  return 0;
}

int barn_getattr_blob(struct stat* stbuf, git_blob* blob, git_repository* repo)
{
  (void)repo;
  stbuf->st_mode = S_IFREG | 0444;
  stbuf->st_nlink = 1;
  stbuf->st_size = git_blob_rawsize(blob);
  return 0;
}
